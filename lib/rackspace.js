const pkgcloud = require( 'pkgcloud' );
const qex      = require( './qex' );

const _rackspace = {
  _client : null,

  _init : function( config ) {
    _rackspace._client = pkgcloud.providers.rackspace.dns.createClient( config );    
  },

  _zone : function( zoneName ) {
    const def = qex.defer();    
    _rackspace._client.getZones( { name: zoneName }, function( err, zones ) {
      if ( err ) {
        def.reject( err );
      } else {
        if ( zones.length > 0 ) {
          def.resolve( zones[0] );
        } else {
          def.reject( new Error( 'Zone not found' ) );
        }
      }
    });
    return def.promise;
  },

  _record : function( zoneId, recordName ) {
    const def = qex.defer();    
    _rackspace._client.getRecords( zoneId, function( err, records ) {
      if ( err ) {
        def.reject( err );
      } else {
        const matched = records.filter( record => record.name === recordName & record.type == 'A' );
        if ( matched.length === 0 ) {
          def.reject( new Error( 'Record not found' ) );
        } else {
          def.resolve( matched[0] );
        }        
      }
    });
    return def.promise;
  },


  _recordByID : function( zoneId, recordId ) {
    const def = qex.defer();
    _rackspace._client.getRecord( zoneId, recordId, function( err, record ) {
      if ( err ) {
        def.reject( err );
      } else {
        def.resolve( record );
      }
    });
    return def.promise;
  },

  _updateRecordData : function( zoneId, record, newData ) {
    const def = qex.defer();
    const updatedRecord = { id: record.id, name: record.name, type: record.type, data: newData };
    _rackspace._client.updateRecord( zoneId, updatedRecord, function( err, response ) {
      if ( err ) {
        def.reject( err );
      } else {
        def.resolve( response );
      }
    });
    return def.promise;
  },


};
module.exports = _rackspace;