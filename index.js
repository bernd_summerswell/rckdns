const rackspace = require( './lib/rackspace' );
const cmdline   = require( './lib/cmdline' );
const qex       = require( './lib/qex' );
const extIP     = require( 'ext-ip' )();


const _main = {
  userName : "",
  apiKey   : "",
  zoneId   : -1,
  recordId : -1,
  publicIP : "",
  zoneName : "",
  recordName : "",
  record     : null,

  init : function() {
    _main.checkCommandLine();
    rackspace._init( { username: _main.userName, apiKey: _main.apiKey } );
  },

  checkCommandLine : function() {
    if ( !( cmdline.hasOption( 'username' ) && cmdline.hasOption( 'apikey' ) ) ) {
      console.error( 'Username and/or APIKey not passed' );
      process.exit();
    }

    _main.userName = cmdline.optionValue( 'username' );
    _main.apiKey   = cmdline.optionValue( 'apikey' );

    _main.zoneId    = ( cmdline.hasOption( 'zoneid' ) ) ? cmdline.optionValue( 'zoneid' ) : -1;
    _main.recordId  = ( cmdline.hasOption( 'recordid' ) ) ? cmdline.optionValue( 'recordid' ) : -1;
    _main.publicIP  = ( cmdline.hasOption( 'publicip' ) ) ? cmdline.optionValue( 'publicip' ) : "";      

    _main.zoneName    = ( cmdline.hasOption( 'zonename' ) ) ? cmdline.optionValue( 'zonename' ) : "";
    _main.recordName  = ( cmdline.hasOption( 'recordname' ) ) ? cmdline.optionValue( 'recordname' ) : "";

    if ( ( _main.zoneId === -1 ) && ( _main.zoneName === "" ) ) {
      console.error( 'Pass either zoneid or zonename' );
      process.exit();
    }

    if ( ( _main.recordId === -1 ) && ( _main.recordName === "" ) ) {
      console.error( 'Pass either recordid or recordname' );
      process.exit();
    }
  },

  getPublicIP : function() {
    if ( _main.publicIP === "" ) {
      return extIP.get()
        .then( ip => {          
          _main.publicIP = ip;
          return ip;
        })
    } else {
      return qex.justReturn(_main.publicIP );
    }
  },

  getZoneId : function() {
    if ( _main.zoneId === -1 ) {      
      return rackspace._zone( {name: _main.zoneName } )
        .then( zone => {
          _main.zoneId = zone.id;
          return _main.zoneId;
        })
    } else {
      return qex.justReturn( _main.zoneId );
    }
  },

  getRecordId : function() {
    if ( _main.recordId === -1 ) {
      return rackspace._record( _main.zoneId.toString(), _main.recordName )
        .then( record => {
          _main.recordId = record.id;
          _main.record = record;
          return _main.recordId;
        })
    } else {
      return rackspace._recordByID( _main.zoneId.toString(), _main.recordId )
        .then( record => {
          _main.recordId = record.id;
          _main.record   = record;
          return _main.recordId;
        });      
    }
  },
}


/**
 * Check the command line options
 */

_main.init();
return qex.runSequence([
  [ _main.getPublicIP ],
  [ _main.getZoneId ],
  [ _main.getRecordId ],
])
  .then( results => {
    let [ publicIP, zoneId, recordId] = [...results];
    console.log( `Public IP: ${_main.publicIP}` );
    console.log( `Zone ID  : ${_main.zoneId}` );
    console.log( `Record ID: ${_main.recordId}` );
    console.log( `Current Record: ${_main.record.data}` );

    console.log( 'Updating...' );
    return rackspace._updateRecordData( _main.zoneId.toString(), _main.record, _main.publicIP );
  })
  .then( () => {
    console.log( 'Updated it' );
    return;
  })
  .catch( err => {
    console.error( err );
  })
  .finally( () => {
    process.exit();
  });
