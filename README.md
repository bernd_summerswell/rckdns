# rckdns

Rackspace DNS updater.


Command line options:

* `--username=<Rackspace Username>` - The Rackspace username to use 
* `--apikey=<Rackspace API Key>` - The Rackspace API for the username

If the specific Zone ID and Record ID are known, they can be passed:
* `--zoneid=<Rackspace DNS Zone ID>` - Rackspace DNS Zone ID
* `--recordid=<Rackspace DNS Record ID>` - Rackspace DNS Record ID

Otherwise the Zone Name and Record Name can be passed:
* `--zonename=<domain.com>` - Domain Zone to be updated
* `--recordname=<subdomain.domain.com>` - Domain Record to be updated

By default the public IP address will be determined, but it can also be passed:
* `--publicip=<PublicIP>` - IP Address the Domain Record will be updated to.



Running the following:
```
node index.js --username=testuser \ 
              --apikey=3428749837492874 \
              --zonename=domain.com \
              --recordname=subdomain.domain.com
```
Will give the following output:              
```
Public IP: 123.123.123.123
Zone ID  : 1234567
Record ID: A-12343535
Current Record: 111.111.111.111
Updating...
Updated it
```
    
